module.exports = {
    mode: "development",
    watch: true,
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules|bower_components/,
                use: { 
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
}