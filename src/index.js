const palettes = require('nice-color-palettes/1000');

console.log(palettes);

var canvas = document.querySelector('canvas')
canvas.width = window.innerWidth
canvas.height = window.innerHeight
var c = canvas.getContext('2d')

// c.fillStyle = 'rgba(255, 0, 0, 0.5)'
// c.fillRect(100, 100, 100, 100)

// c.beginPath()
// c.moveTo(50, 300)
// c.lineTo(300, 50)
// c.strokeStyle = "#fa34a3"
// c.stroke()

// c.beginPath()
// c.arc(300, 300, 30, 0, Math.PI * 2, false)
// c.stroke()

// const createCircle = (xPos,yPos,radius) => {
//  radius = radius < 1 ? 1 : radius
//  xPos = xPos < radius ? radius + 1 : xPos > window.innerWidth - radius ? window.innerWidth - radius - 1 : xPos
// //  yPos = yPos < radius ? radius + 1 : yPos
//  yPos = yPos > window.innerHeight - radius ? window.innerHeight - radius - 1 : yPos < radius ? radius + 1 : yPos
//  c.beginPath()
//  c.arc(xPos, yPos, radius, 0, Math.PI * 2,false)
//  c.stroke()
// }

// const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max))
// // const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max))

// const createLotsOfCircles = (count) => {
//     for (let i = 0; i < count; i++) {
//         createCircle(Math.random() * window.innerWidth, Math.random() * window.innerHeight, getRandomInt(50), 0)
//         // createCircle(getRandomInt(400), getRandomInt(400), getRandomInt(50), 0)
//     }
// }

// createLotsOfCircles(100)

let circleArray = [];

const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max));
const colorPalette = palettes[getRandomInt(1000)];
console.log(colorPalette);

class Circle {
    constructor(x, y, dx, dy, radius, color) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.radius = radius;
        this.color = color;
    }

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.stroke();
        c.fillStyle = this.color;
        c.fill();
    } 

    update() {
        if (this.x > innerWidth - this.radius || this.x < this.radius) {
            this.dx = -this.dx;
        }
    
        if (this.y > innerHeight - this.radius || this.y < this.radius) {
            this.dy = -this.dy;
        }

        this.x += this.dx;
        this.y += this.dy;

        this.draw();
    }
}

for (let i = 0; i < 150; i++) {
    const radius = 30;
    let x = getRandomX(radius);
    let y = getRandomY(radius);
    let dx = Math.random() - 0.5;
    let dy = Math.random() - 0.5;
    
    circleArray.push(new Circle(x, y, dx, dy, radius, colorPalette[getRandomInt(5)]));
}

function getRandomX(radius) {
    let x = Math.random() * window.innerWidth;
    
    if (x < radius) {
        return radius + 1;
    } else if (x > window.innerWidth - radius) {
        return window.innerWidth - radius - 1;
    } else {
        return x;
    }
}

function getRandomY(radius) {
    let y = Math.random() * window.innerHeight;
    if (y < radius) {
        return radius + 1;
    } else if (y > window.innerHeight - radius) {
        return window.innerHeight - radius - 1;
    } else {
        return y;
    }    
}

const animate = () => {
    c.clearRect(0, 0, innerWidth, innerHeight)

    circleArray.forEach(circle => circle.update());
    
    requestAnimationFrame(animate);
}

animate();